// Model scale
mScale = 1;

// Circular resolution
cRes = 360;

// Raw dimensions
modelHeight = mScale * 30;  // Original = 30
modelDepth  = mScale * 5;   // Original = 5
modelWidth  = mScale * 20;  // Original = 20
railHeight  = mScale * 2;   // Original = 2

// Calculated dimensions
railSlotDepth   = (modelDepth - modelDepth / 5 * 2) / 2;
loopRadius      = modelWidth / 2;
slotRadius      = loopRadius / 2;

// Model
difference() {
    // Body
    cube(size = [modelWidth, modelDepth, modelHeight]);

    // Rail cutouts
    translate([0, 0, railHeight]) {
        cube([modelWidth, railSlotDepth, railHeight]);
    }
    translate([0, modelDepth - railSlotDepth, railHeight]) {
        cube([modelWidth, railSlotDepth, railHeight]);
    }

    // Loop slot
    translate([modelWidth / 2, 0, modelHeight - loopRadius]) {
        rotate([270, 0, 0]) {
            slot(radius = slotRadius, innerLength = slotRadius, depth = modelDepth);
        }
    }

    // Loop corners
    difference() {
        translate([0, 0, modelHeight - loopRadius]) {
            cube([modelWidth, modelDepth, loopRadius]);
        }
        translate([loopRadius, 0, modelHeight - loopRadius]) {
            rotate([270, 0, 0]) {
                cylinder(h = modelDepth, r1 = loopRadius, r2 = loopRadius, $fn = cRes);
            }
        }
    }
}

// Modules
{
    /**
     * Creates the body of a slot on the xz plane with the length along the z axis and the lower
     * focus at [0, depth / 2, 0].
     *
     * Params:
     *  - radius        : number    The radius of the slot.
     *  - innerLength   : number    The length between the foci of the slot.
     *  - depth         : number    The depth of the slot.
     */
    module slot(radius, innerLength, depth) {
        union() {
            cylinder(h = depth, r1 = radius, r2 = radius, $fn = cRes);
            translate([-radius, 0, 0]) {
                cube(size = [radius * 2, innerLength, depth]);
            }
            translate([0, innerLength, 0]) {
                cylinder(h = depth, r1 = radius, r2 = radius, $fn = cRes);
            }
        }
    }
}
